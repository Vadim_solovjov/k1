import java.util.Arrays;

public class ColorSort {

   enum Color {red, green, blue}

   public static void main(String[] param) {
      Color[] balls = {Color.blue, Color.red, Color.green, Color.blue, Color.red, Color.green, Color.blue, Color.red, Color.green};
      System.out.println(Arrays.toString(balls)); //kirjutame värvid juhuslikult
      reorder(balls);//meetod sorteerib pallid punaseks - roheliseks - siniseks
      System.out.println(Arrays.toString(balls));
   }

   public static void reorder(Color[] balls) {
      int red = 0, green = 0, blue = 0;
      for (Color ball : balls) {
         switch (ball) {
            case red:
               red++;
               break;
            case green:
               green++;
               break;
            case blue:
               blue++;
               break; //Tsükkel töötab, leiab värvi ja lisab selle uude plokki ja läheb tagasi ja nii see toimib
         }
      }
      for (int i = 0; i < red; i++) {
         balls[i] = Color.red; //sorteerib kõik punased pallid vasakult
      }
      for (int i = red; i < red + green; i++) {
         balls[i] = Color.green; //sorteerib kõik rohelised pallid keskel
      }
      for (int i = red + green; i < red + green + blue; i++) {
         balls[i] = Color.blue; //sorteerib kõik rohelised sinised paremalt
      }
   }
}